document.addEventListener('DOMContentLoaded', function() {
    const fileInput = document.getElementById('file-input');
    fileInput.addEventListener('change', enableEncryption);
    const binarizationAlgorithmInputs = document.querySelectorAll('input[name="binarization-algorithm"]');
    for (i = 0; i < binarizationAlgorithmInputs.length; i++) {
        binarizationAlgorithmInputs[i].addEventListener('change', enableEncryption);
    }
    const encryptButton = document.getElementById('encrypt-button');
    encryptButton.addEventListener('click', main);
    const downloadButton = document.getElementById('download-button');
    downloadButton.addEventListener('click', function() {
        rawRandomData1Link.click();
        rawRandomData2Link.click();
    });
});

const canvas = document.createElement('canvas');
const context = canvas.getContext('2d');
var secretImage = null;
var secretData = null;

function encryptImage(canvas, context, imageData) {
    canvas.width = 2 * imageData.width;
    canvas.height = 2 * imageData.height;
    const randomData1 = context.createImageData(2 * imageData.width, 2 * imageData.height);
    const randomData2 = context.createImageData(2 * imageData.width, 2 * imageData.height);
    for (j = 0; j < imageData.data.length; j += 4 * imageData.width) {
        for (i = 0; i < 4 * imageData.width; i += 4) {
            const choice = Math.floor(Math.random() * 6);
            if (imageData.data[j + i] === 0) {
                switch (choice) {
                    case 0:
                        updateBlock(randomData1, i, j, [255, 0, 0, 255]);
                        updateBlock(randomData2, i, j, [0, 255, 255, 0]);
                        break;
                    case 1:
                        updateBlock(randomData1, i, j, [0, 255, 255, 0]);
                        updateBlock(randomData2, i, j, [255, 0, 0, 255]);
                        break;
                    case 2:
                        updateBlock(randomData1, i, j, [0, 255, 0, 255]);
                        updateBlock(randomData2, i, j, [255, 0, 255, 0]);
                        break;
                    case 3:
                        updateBlock(randomData1, i, j, [255, 0, 255, 0]);
                        updateBlock(randomData2, i, j, [0, 255, 0, 255]);
                        break;
                    case 4:
                        updateBlock(randomData1, i, j, [0, 0, 255, 255]);
                        updateBlock(randomData2, i, j, [255, 255, 0, 0]);
                        break;
                    case 5:
                        updateBlock(randomData1, i, j, [255, 255, 0, 0]);
                        updateBlock(randomData2, i, j, [0, 0, 255, 255]);
                        break;
                }
            }
            else {
                switch (choice) {
                    case 0:
                        updateBlock(randomData1, i, j, [255, 0, 0, 255]);
                        updateBlock(randomData2, i, j, [255, 0, 0, 255]);
                        break;
                    case 1:
                        updateBlock(randomData1, i, j, [0, 255, 255, 0]);
                        updateBlock(randomData2, i, j, [0, 255, 255, 0]);
                        break;
                    case 2:
                        updateBlock(randomData1, i, j, [0, 255, 0, 255]);
                        updateBlock(randomData2, i, j, [0, 255, 0, 255]);
                        break;
                    case 3:
                        updateBlock(randomData1, i, j, [255, 0, 255, 0]);
                        updateBlock(randomData2, i, j, [255, 0, 255, 0]);
                        break;
                    case 4:
                        updateBlock(randomData1, i, j, [0, 0, 255, 255]);
                        updateBlock(randomData2, i, j, [0, 0, 255, 255]);
                        break;
                    case 5:
                        updateBlock(randomData1, i, j, [255, 255, 0, 0]);
                        updateBlock(randomData2, i, j, [255, 255, 0, 0]);
                        break;
                }
            }
        }
    }
    return [randomData1, randomData2];
}

const rawRandomData1Link = document.createElement('a');
rawRandomData1Link.download = "image-aleatoire-1.png";
const rawRandomData2Link = document.createElement('a');
rawRandomData2Link.download = "image-aleatoire-2.png";

function main() {
    this.disabled = true;
    document.getElementById('output').hidden = false;
    const rawRandomData = encryptImage(canvas, context, secretData);
    context.putImageData(rawRandomData[0], 0, 0);
    rawRandomData1Link.href = canvas.toDataURL();
    context.putImageData(rawRandomData[1], 0, 0);
    rawRandomData2Link.href = canvas.toDataURL();
    const imagesFrame = document.getElementById('images');
    if (2 * secretData.width / imagesFrame.clientWidth <= 1 && 2 * secretData.height / imagesFrame.clientHeight <= 1) {
        context.putImageData(rawRandomData[0], 0, 0);
        const rawRandomImage1 = new Image();
        rawRandomImage1.src = canvas.toDataURL();
        context.putImageData(rawRandomData[1], 0, 0);
        const rawRandomImage2 = new Image();
        rawRandomImage2.src = canvas.toDataURL();
        displayImages(rawRandomImage1, rawRandomImage2);
    }
    else {
        let scaledRandomData = null;
        const ratio = secretData.width / secretData.height;
        if (2 * secretData.width / imagesFrame.clientWidth >= 2 * secretData.height / imagesFrame.clientHeight) {
            const scaledSecretData = binarizeImage(canvas, context, secretImage, Math.floor(imagesFrame.clientWidth / 2), Math.floor((imagesFrame.clientWidth / 2) / ratio), document.querySelector('input[name="binarization-algorithm"]:checked').value, "secret-image", false);
            scaledRandomData = encryptImage(canvas, context, scaledSecretData);
        }
        else {
            const scaledSecretData = binarizeImage(canvas, context, secretImage, Math.floor((imagesFrame.clientHeight / 2) * ratio), Math.floor(imagesFrame.clientHeight / 2), document.querySelector('input[name="binarization-algorithm"]:checked').value, "secret-image", false);
            scaledRandomData = encryptImage(canvas, context, scaledSecretData);
        }
        context.putImageData(scaledRandomData[0], 0, 0);
        const scaledRandomImage1 = new Image();
        scaledRandomImage1.src = canvas.toDataURL();
        context.putImageData(scaledRandomData[1], 0, 0);
        const scaledRandomImage2 = new Image();
        scaledRandomImage2.src = canvas.toDataURL();
        displayImages(scaledRandomImage1, scaledRandomImage2);
    }
}

function enableEncryption() {
    document.getElementById('output').hidden = true;
    const fileInput = document.getElementById('file-input');
    const encryptButton = document.getElementById('encrypt-button');
    if (fileInput.files.length > 0) {
        switch (this.type) {
            case "file":
                const file = this.files[0];
                const reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onloadend = function(event) {
                    secretImage = new Image();
                    secretImage.src = event.target.result;
                    secretImage.onload = function() {
                        secretData = binarizeImage(canvas, context, secretImage, secretImage.width, secretImage.height, document.querySelector('input[name="binarization-algorithm"]:checked').value, "secret-image", true);
                        encryptButton.disabled = false;
                    }
                }
                break;
            case "radio":
                secretData = binarizeImage(canvas, context, secretImage, secretImage.width, secretImage.height, document.querySelector('input[name="binarization-algorithm"]:checked').value, "secret-image", true);
                encryptButton.disabled = false;
                break;
        }
        
    }
    else {
        document.getElementById('secret-image').innerHTML = "";
        encryptButton.disabled = true;
    }
}