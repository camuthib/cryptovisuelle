document.addEventListener('DOMContentLoaded', function() {
    const imageOpacityInputs = document.getElementsByClassName('image-opacity-input');
    for (i = 0; i < imageOpacityInputs.length; i++) {
        imageOpacityInputs[i].addEventListener('input', updateImageOpacity);
    }
    const rectanglesPairs = document.getElementsByClassName('rectangles');
    for (i = 0; i < rectanglesPairs.length; i++) {
        rectanglesPairs[i].addEventListener('click', updateGround);
    }
    const rectangles = document.getElementsByClassName('rectangle');
    rectangles[0].style.zIndex = "2";
    rectangles[1].style.zIndex = "1";
    rectangles[2].style.zIndex = "2";
    rectangles[3].style.zIndex = "1";
});

function displayImages(image1, image2) {
    document.getElementById('images').innerHTML = "";
    const image1Opacity = document.getElementById('image-1-opacity-input').value;
    image1.style.opacity = image1Opacity;
    document.getElementById('image-1-opacity-value').textContent = image1Opacity;
    image1.id = "image-1";
    image1.draggable = false;
    const image2Opacity = document.getElementById('image-2-opacity-input').value;
    image2.style.opacity = image2Opacity;
    document.getElementById('image-2-opacity-value').textContent = image2Opacity;
    image2.id = "image-2";
    image2.draggable = false;
    image2.style.right = 0;
    image2.style.bottom = 0;
    const rectangles = document.getElementsByClassName('rectangle');
    if (rectangles[0].style.zIndex === "1") {
        image1.style.zIndex = "2";
        image2.style.zIndex = "1";
    }
    else {
        image1.style.zIndex = "1";
        image2.style.zIndex = "2";
    }
    image1.addEventListener('click', enableImageMoving);
    image2.addEventListener('click', enableImageMoving);
    document.getElementById('images').appendChild(image1);
    document.getElementById('images').appendChild(image2);
}

function updateImageOpacity() {
    switch (this.name) {
        case "image-1-opacity":
            document.getElementById('image-1').style.opacity = this.value;
            document.getElementById('image-1-opacity-value').textContent = this.value;
            break;
        case "image-2-opacity":
            document.getElementById('image-2').style.opacity = this.value;
            document.getElementById('image-2-opacity-value').textContent = this.value;
            break;
    }
}

function updateGround() {
    const rectangles = document.getElementsByClassName('rectangle');
    const image1 = document.getElementById('image-1');
    const image2 = document.getElementById('image-2');
    if (rectangles[0].style.zIndex === "1") {
        rectangles[0].style.zIndex = "2";
        rectangles[1].style.zIndex = "1";
        rectangles[2].style.zIndex = "2";
        rectangles[3].style.zIndex = "1";
        image1.style.zIndex = "1";
        image2.style.zIndex = "2";
    }
    else {
        rectangles[0].style.zIndex = "1";
        rectangles[1].style.zIndex = "2";
        rectangles[2].style.zIndex = "1";
        rectangles[3].style.zIndex = "2";
        image1.style.zIndex = "2";
        image2.style.zIndex = "1";
    }
}

var originX = 0;
var originY = 0;
var movingImage = null;
var isMoving = false;

function enableImageMoving(event) {
    if (isMoving === false) {
        event.stopPropagation();
        originX = event.clientX;
        originY = event.clientY;
        movingImage = this;
        isMoving = true;
        const arrows = document.getElementsByClassName('arrows');
        switch (movingImage.id) {
            case "image-1":
                arrows[0].style.visibility = "visible";
                break;
            case "image-2":
                arrows[1].style.visibility = "visible";
                break;
        }
        document.addEventListener('mousemove', imageMovingMouse);
        document.addEventListener('keydown', imageMovingKeyboard);
        document.addEventListener('click', disableImageMoving);
        document.addEventListener('keydown', disableImageMoving);
    }
}

function imageMovingMouse(event) {
    let gapX = event.clientX - originX;
    let gapY = event.clientY - originY;
    originX = event.clientX;
    originY = event.clientY;
    let newLeft = movingImage.offsetLeft + gapX;
    let newTop = movingImage.offsetTop + gapY;
    const imagesFrame = document.getElementById('images');
    if (gapX < 0 && newLeft >= 0 || gapX > 0 && newLeft + movingImage.width <= imagesFrame.clientWidth) {
        movingImage.style.left = newLeft + "px";
    }
    if (gapY < 0 && newTop >= 0 || gapY > 0 && newTop + movingImage.height <= imagesFrame.clientHeight) {
        movingImage.style.top = newTop + "px";
    }
}

function imageMovingKeyboard(event) {
    event.preventDefault();
    const imagesFrame = document.getElementById('images');
    let newLeft = movingImage.offsetLeft;
    let newTop = movingImage.offsetTop;
    switch (event.key) {
        case "ArrowLeft":
            newLeft--;
            if (newLeft >= 0) {
                movingImage.style.left = newLeft + "px";
            }
            break;
        case "ArrowUp":
            newTop--;
            if (newTop >= 0) {
                movingImage.style.top = newTop + "px";
            }
            break;
        case "ArrowDown":
            newTop++;
            if (newTop + movingImage.height <= imagesFrame.clientHeight) {
                movingImage.style.top = newTop + "px";
            }
            break;
        case "ArrowRight":
            newLeft++;
            if (newLeft + movingImage.width <= imagesFrame.clientWidth) {
                movingImage.style.left = newLeft + "px";
            }
            break;
    }
}

function disableImageMoving(event) {
    switch (event.type) {
        case "keydown":
            if (event.key !== "Enter") {
                break;
            }
        case "click":
            isMoving = false;
            const arrows = document.getElementsByClassName('arrows');
            for (i = 0; i < arrows.length; i++) {
                arrows[i].style.visibility = "hidden";
            }
            document.removeEventListener('mousemove', imageMovingMouse);
            document.removeEventListener('keydown', imageMovingKeyboard);
            break;
    }
}