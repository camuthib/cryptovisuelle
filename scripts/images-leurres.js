document.addEventListener('DOMContentLoaded', function() {
    const fileInputs = document.querySelectorAll('input[type=file]');
    for (i = 0; i < fileInputs.length; i++) {
        fileInputs[i].addEventListener('change', enableEncryption);
    }
    const binarizationAlgorithmInputs = document.querySelectorAll('input[type=radio]');
    for (i = 0; i < binarizationAlgorithmInputs.length; i++) {
        binarizationAlgorithmInputs[i].addEventListener('change', enableEncryption);
    }
    const encryptButton = document.getElementById('encrypt-button');
    encryptButton.addEventListener('click', main);
    const downloadButton = document.getElementById('download-button');
    downloadButton.addEventListener('click', function() {
        rawEncryptedData1Link.click();
        rawEncryptedData2Link.click();
    });
});

const canvas = document.createElement('canvas');
const context = canvas.getContext('2d');
var secretImage = null;
var secretData = null;
var lureImage1 = null;
var lureData1 = null;
var lureImage2 = null;
var lureData2 = null;

function encryptImage(canvas, context, secretData, lureData1, lureData2) {
    canvas.width = 2 * secretData.width;
    canvas.height = 2 * secretData.height;
    const encryptedData1 = context.createImageData(2 * secretData.width, 2 * secretData.height);
    const encryptedData2 = context.createImageData(2 * secretData.width, 2 * secretData.height);
    for (j = 0; j < secretData.data.length; j += 4 * secretData.width) {
        for (i = 0; i < 4 * secretData.width; i += 4) {
            if (secretData.data[j + i] === 0) {
                if (lureData1.data[j + i] === 0) {
                    if (lureData2.data[j + i] === 0) {
                        const choice = Math.floor(Math.random() * 12);
                        switch (choice) {
                            case 0:
                                updateBlock(encryptedData1, i, j, [255, 0, 0, 0]);
                                updateBlock(encryptedData2, i, j, [0, 255, 0, 0]);
                                break;
                            case 1:
                                updateBlock(encryptedData1, i, j, [255, 0, 0, 0]);
                                updateBlock(encryptedData2, i, j, [0, 0, 255, 0]);
                                break;
                            case 2:
                                updateBlock(encryptedData1, i, j, [255, 0, 0, 0]);
                                updateBlock(encryptedData2, i, j, [0, 0, 0, 255]);
                                break;
                            case 3:
                                updateBlock(encryptedData1, i, j, [0, 255, 0, 0]);
                                updateBlock(encryptedData2, i, j, [255, 0, 0, 0]);
                                break;
                            case 4:
                                updateBlock(encryptedData1, i, j, [0, 255, 0, 0]);
                                updateBlock(encryptedData2, i, j, [0, 0, 255, 0]);
                                break;
                            case 5:
                                updateBlock(encryptedData1, i, j, [0, 255, 0, 0]);
                                updateBlock(encryptedData2, i, j, [0, 0, 0, 255]);
                                break;
                            case 6:
                                updateBlock(encryptedData1, i, j, [0, 0, 255, 0]);
                                updateBlock(encryptedData2, i, j, [255, 0, 0, 0]);
                                break;
                            case 7:
                                updateBlock(encryptedData1, i, j, [0, 0, 255, 0]);
                                updateBlock(encryptedData2, i, j, [0, 255, 0, 0]);
                                break;
                            case 8:
                                updateBlock(encryptedData1, i, j, [0, 0, 255, 0]);
                                updateBlock(encryptedData2, i, j, [0, 0, 0, 255]);
                                break;
                            case 9:
                                updateBlock(encryptedData1, i, j, [0, 0, 0, 255]);
                                updateBlock(encryptedData2, i, j, [255, 0, 0, 0]);
                                break;
                            case 10:
                                updateBlock(encryptedData1, i, j, [0, 0, 0, 255]);
                                updateBlock(encryptedData2, i, j, [0, 255, 0, 0]);
                                break;
                            case 11:
                                updateBlock(encryptedData1, i, j, [0, 0, 0, 255]);
                                updateBlock(encryptedData2, i, j, [0, 0, 255, 0]);
                                break;
                        }
                    }
                    else {
                        const choice = Math.floor(Math.random() * 12);
                        switch (choice) {
                            case 0:
                                updateBlock(encryptedData1, i, j, [255, 0, 0, 0]);
                                updateBlock(encryptedData2, i, j, [0, 255, 255, 0]);
                                break;
                            case 1:
                                updateBlock(encryptedData1, i, j, [255, 0, 0, 0]);
                                updateBlock(encryptedData2, i, j, [0, 0, 255, 255]);
                                break;
                            case 2:
                                updateBlock(encryptedData1, i, j, [255, 0, 0, 0]);
                                updateBlock(encryptedData2, i, j, [0, 255, 0, 255]);
                                break;
                            case 3:
                                updateBlock(encryptedData1, i, j, [0, 255, 0, 0]);
                                updateBlock(encryptedData2, i, j, [255, 0, 255, 0]);
                                break;
                            case 4:
                                updateBlock(encryptedData1, i, j, [0, 255, 0, 0]);
                                updateBlock(encryptedData2, i, j, [0, 0, 255, 255]);
                                break;
                            case 5:
                                updateBlock(encryptedData1, i, j, [0, 255, 0, 0]);
                                updateBlock(encryptedData2, i, j, [255, 0, 0, 255]);
                                break;
                            case 6:
                                updateBlock(encryptedData1, i, j, [0, 0, 255, 0]);
                                updateBlock(encryptedData2, i, j, [255, 255, 0, 0]);
                                break;
                            case 7:
                                updateBlock(encryptedData1, i, j, [0, 0, 255, 0]);
                                updateBlock(encryptedData2, i, j, [0, 255, 0, 255]);
                                break;
                            case 8:
                                updateBlock(encryptedData1, i, j, [0, 0, 255, 0]);
                                updateBlock(encryptedData2, i, j, [255, 0, 0, 255]);
                                break;
                            case 9:
                                updateBlock(encryptedData1, i, j, [0, 0, 0, 255]);
                                updateBlock(encryptedData2, i, j, [255, 255, 0, 0]);
                                break;
                            case 10:
                                updateBlock(encryptedData1, i, j, [0, 0, 0, 255]);
                                updateBlock(encryptedData2, i, j, [0, 255, 255, 0]);
                                break;
                            case 11:
                                updateBlock(encryptedData1, i, j, [0, 0, 0, 255]);
                                updateBlock(encryptedData2, i, j, [255, 0, 255, 0]);
                                break;
                        }
                    }
                }
                else {
                    if (lureData2.data[j + i] === 0) {
                        const choice = Math.floor(Math.random() * 12);
                        switch (choice) {
                            case 0:
                                updateBlock(encryptedData1, i, j, [0, 255, 255, 0]);
                                updateBlock(encryptedData2, i, j, [255, 0, 0, 0]);
                                break;
                            case 1:
                                updateBlock(encryptedData1, i, j, [0, 0, 255, 255]);
                                updateBlock(encryptedData2, i, j, [255, 0, 0, 0]);
                                break;
                            case 2:
                                updateBlock(encryptedData1, i, j, [0, 255, 0, 255]);
                                updateBlock(encryptedData2, i, j, [255, 0, 0, 0]);
                                break;
                            case 3:
                                updateBlock(encryptedData1, i, j, [255, 0, 255, 0]);
                                updateBlock(encryptedData2, i, j, [0, 255, 0, 0]);
                                break;
                            case 4:
                                updateBlock(encryptedData1, i, j, [0, 0, 255, 255]);
                                updateBlock(encryptedData2, i, j, [0, 255, 0, 0]);
                                break;
                            case 5:
                                updateBlock(encryptedData1, i, j, [255, 0, 0, 255]);
                                updateBlock(encryptedData2, i, j, [0, 255, 0, 0]);
                                break;
                            case 6:
                                updateBlock(encryptedData1, i, j, [255, 255, 0, 0]);
                                updateBlock(encryptedData2, i, j, [0, 0, 255, 0]);
                                break;
                            case 7:
                                updateBlock(encryptedData1, i, j, [0, 255, 0, 255]);
                                updateBlock(encryptedData2, i, j, [0, 0, 255, 0]);
                                break;
                            case 8:
                                updateBlock(encryptedData1, i, j, [255, 0, 0, 255]);
                                updateBlock(encryptedData2, i, j, [0, 0, 255, 0]);
                                break;
                            case 9:
                                updateBlock(encryptedData1, i, j, [255, 255, 0, 0]);
                                updateBlock(encryptedData2, i, j, [0, 0, 0, 255]);
                                break;
                            case 10:
                                updateBlock(encryptedData1, i, j, [0, 255, 255, 0]);
                                updateBlock(encryptedData2, i, j, [0, 0, 0, 255]);
                                break;
                            case 11:
                                updateBlock(encryptedData1, i, j, [255, 0, 255, 0]);
                                updateBlock(encryptedData2, i, j, [0, 0, 0, 255]);
                                break;
                        }
                    }
                    else {
                        const choice = Math.floor(Math.random() * 6);
                        switch (choice) {
                            case 0:
                                updateBlock(encryptedData1, i, j, [255, 255, 0, 0]);
                                updateBlock(encryptedData2, i, j, [0, 0, 255, 255]);
                                break;
                            case 1:
                                updateBlock(encryptedData1, i, j, [0, 255, 255, 0]);
                                updateBlock(encryptedData2, i, j, [255, 0, 0, 255]);
                                break;
                            case 2:
                                updateBlock(encryptedData1, i, j, [0, 0, 255, 255]);
                                updateBlock(encryptedData2, i, j, [255, 255, 0, 0]);
                                break;
                            case 3:
                                updateBlock(encryptedData1, i, j, [255, 0, 0, 255]);
                                updateBlock(encryptedData2, i, j, [0, 255, 255, 0]);
                                break;
                            case 4:
                                updateBlock(encryptedData1, i, j, [255, 0, 255, 0]);
                                updateBlock(encryptedData2, i, j, [0, 255, 0, 255]);
                                break;
                            case 5:
                                updateBlock(encryptedData1, i, j, [0, 255, 0, 255]);
                                updateBlock(encryptedData2, i, j, [255, 0, 255, 0]);
                                break;
                        }
                    }
                }
            }
            else {
                if (lureData1.data[j + i] === 0) {
                    if (lureData2.data[j + i] === 0) {
                        const choice = Math.floor(Math.random() * 4);
                        switch (choice) {
                            case 0:
                                updateBlock(encryptedData1, i, j, [255, 0, 0, 0]);
                                updateBlock(encryptedData2, i, j, [255, 0, 0, 0]);
                                break;
                            case 1:
                                updateBlock(encryptedData1, i, j, [0, 255, 0, 0]);
                                updateBlock(encryptedData2, i, j, [0, 255, 0, 0]);
                                break;
                            case 2:
                                updateBlock(encryptedData1, i, j, [0, 0, 255, 0]);
                                updateBlock(encryptedData2, i, j, [0, 0, 255, 0]);
                                break;
                            case 3:
                                updateBlock(encryptedData1, i, j, [0, 0, 0, 255]);
                                updateBlock(encryptedData2, i, j, [0, 0, 0, 255]);
                                break;
                        }
                    }
                    else {
                        const choice = Math.floor(Math.random() * 12);
                        switch (choice) {
                            case 0:
                                updateBlock(encryptedData1, i, j, [255, 0, 0, 0]);
                                updateBlock(encryptedData2, i, j, [255, 255, 0, 0]);
                                break;
                            case 1:
                                updateBlock(encryptedData1, i, j, [255, 0, 0, 0]);
                                updateBlock(encryptedData2, i, j, [255, 0, 255, 0]);
                                break;
                            case 2:
                                updateBlock(encryptedData1, i, j, [255, 0, 0, 0]);
                                updateBlock(encryptedData2, i, j, [255, 0, 0, 255]);
                                break;
                            case 3:
                                updateBlock(encryptedData1, i, j, [0, 255, 0, 0]);
                                updateBlock(encryptedData2, i, j, [255, 255, 0, 0]);
                                break;
                            case 4:
                                updateBlock(encryptedData1, i, j, [0, 255, 0, 0]);
                                updateBlock(encryptedData2, i, j, [0, 255, 255, 0]);
                                break;
                            case 5:
                                updateBlock(encryptedData1, i, j, [0, 255, 0, 0]);
                                updateBlock(encryptedData2, i, j, [0, 255, 0, 255]);
                                break;
                            case 6:
                                updateBlock(encryptedData1, i, j, [0, 0, 255, 0]);
                                updateBlock(encryptedData2, i, j, [255, 0, 255, 0]);
                                break;
                            case 7:
                                updateBlock(encryptedData1, i, j, [0, 0, 255, 0]);
                                updateBlock(encryptedData2, i, j, [0, 255, 255, 0]);
                                break;
                            case 8:
                                updateBlock(encryptedData1, i, j, [0, 0, 255, 0]);
                                updateBlock(encryptedData2, i, j, [0, 0, 255, 255]);
                                break;
                            case 9:
                                updateBlock(encryptedData1, i, j, [0, 0, 0, 255]);
                                updateBlock(encryptedData2, i, j, [255, 0, 0, 255]);
                                break;
                            case 10:
                                updateBlock(encryptedData1, i, j, [0, 0, 0, 255]);
                                updateBlock(encryptedData2, i, j, [0, 255, 0, 255]);
                                break;
                            case 11:
                                updateBlock(encryptedData1, i, j, [0, 0, 0, 255]);
                                updateBlock(encryptedData2, i, j, [0, 0, 255, 255]);
                                break;
                        }
                    }
                }
                else {
                    if (lureData2.data[j + i] === 0) {
                        const choice = Math.floor(Math.random() * 12);
                        switch (choice) {
                            case 0:
                                updateBlock(encryptedData1, i, j, [255, 255, 0, 0]);
                                updateBlock(encryptedData2, i, j, [255, 0, 0, 0]);
                                break;
                            case 1:
                                updateBlock(encryptedData1, i, j, [255, 0, 255, 0]);
                                updateBlock(encryptedData2, i, j, [255, 0, 0, 0]);
                                break;
                            case 2:
                                updateBlock(encryptedData1, i, j, [255, 0, 0, 255]);
                                updateBlock(encryptedData2, i, j, [255, 0, 0, 0]);
                                break;
                            case 3:
                                updateBlock(encryptedData1, i, j, [255, 255, 0, 0]);
                                updateBlock(encryptedData2, i, j, [0, 255, 0, 0]);
                                break;
                            case 4:
                                updateBlock(encryptedData1, i, j, [0, 255, 255, 0]);
                                updateBlock(encryptedData2, i, j, [0, 255, 0, 0]);
                                break;
                            case 5:
                                updateBlock(encryptedData1, i, j, [0, 255, 0, 255]);
                                updateBlock(encryptedData2, i, j, [0, 255, 0, 0]);
                                break;
                            case 6:
                                updateBlock(encryptedData1, i, j, [255, 0, 255, 0]);
                                updateBlock(encryptedData2, i, j, [0, 0, 255, 0]);
                                break;
                            case 7:
                                updateBlock(encryptedData1, i, j, [0, 255, 255, 0]);
                                updateBlock(encryptedData2, i, j, [0, 0, 255, 0]);
                                break;
                            case 8:
                                updateBlock(encryptedData1, i, j, [0, 0, 255, 255]);
                                updateBlock(encryptedData2, i, j, [0, 0, 255, 0]);
                                break;
                            case 9:
                                updateBlock(encryptedData1, i, j, [255, 0, 0, 255]);
                                updateBlock(encryptedData2, i, j, [0, 0, 0, 255]);
                                break;
                            case 10:
                                updateBlock(encryptedData1, i, j, [0, 255, 0, 255]);
                                updateBlock(encryptedData2, i, j, [0, 0, 0, 255]);
                                break;
                            case 11:
                                updateBlock(encryptedData1, i, j, [0, 0, 255, 255]);
                                updateBlock(encryptedData2, i, j, [0, 0, 0, 255]);
                                break;
                        }
                    }
                    else {
                        const choice = Math.floor(Math.random() * 24);
                        switch (choice) {
                            case 0:
                                updateBlock(encryptedData1, i, j, [255, 255, 0, 0]);
                                updateBlock(encryptedData2, i, j, [255, 0, 255, 0]);
                                break;
                            case 1:
                                updateBlock(encryptedData1, i, j, [255, 255, 0, 0]);
                                updateBlock(encryptedData2, i, j, [255, 0, 0, 255]);
                                break;
                            case 2:
                                updateBlock(encryptedData1, i, j, [255, 0, 255, 0]);
                                updateBlock(encryptedData2, i, j, [255, 255, 0, 0]);
                                break;
                            case 3:
                                updateBlock(encryptedData1, i, j, [255, 0, 255, 0]);
                                updateBlock(encryptedData2, i, j, [255, 0, 0, 255]);
                                break;
                            case 4:
                                updateBlock(encryptedData1, i, j, [255, 0, 0, 255]);
                                updateBlock(encryptedData2, i, j, [255, 255, 0, 0]);
                                break;
                            case 5:
                                updateBlock(encryptedData1, i, j, [255, 0, 0, 255]);
                                updateBlock(encryptedData2, i, j, [255, 0, 255, 0]);
                                break;
                            case 6:
                                updateBlock(encryptedData1, i, j, [255, 255, 0, 0]);
                                updateBlock(encryptedData2, i, j, [0, 255, 255, 0]);
                                break;
                            case 7:
                                updateBlock(encryptedData1, i, j, [255, 255, 0, 0]);
                                updateBlock(encryptedData2, i, j, [0, 255, 0, 255]);
                                break;
                            case 8:
                                updateBlock(encryptedData1, i, j, [0, 255, 255, 0]);
                                updateBlock(encryptedData2, i, j, [255, 255, 0, 0]);
                                break;
                            case 9:
                                updateBlock(encryptedData1, i, j, [0, 255, 255, 0]);
                                updateBlock(encryptedData2, i, j, [0, 255, 0, 255]);
                                break;
                            case 10:
                                updateBlock(encryptedData1, i, j, [0, 255, 0, 255]);
                                updateBlock(encryptedData2, i, j, [255, 255, 0, 0]);
                                break;
                            case 11:
                                updateBlock(encryptedData1, i, j, [0, 255, 0, 255]);
                                updateBlock(encryptedData2, i, j, [0, 255, 255, 0]);
                                break;
                            case 12:
                                updateBlock(encryptedData1, i, j, [255, 0, 255, 0]);
                                updateBlock(encryptedData2, i, j, [0, 255, 255, 0]);
                                break;
                            case 13:
                                updateBlock(encryptedData1, i, j, [255, 0, 255, 0]);
                                updateBlock(encryptedData2, i, j, [0, 0, 255, 255]);
                                break;
                            case 14:
                                updateBlock(encryptedData1, i, j, [0, 255, 255, 0]);
                                updateBlock(encryptedData2, i, j, [255, 0, 255, 0]);
                                break;
                            case 15:
                                updateBlock(encryptedData1, i, j, [0, 255, 255, 0]);
                                updateBlock(encryptedData2, i, j, [0, 0, 255, 255]);
                                break;
                            case 16:
                                updateBlock(encryptedData1, i, j, [0, 0, 255, 255]);
                                updateBlock(encryptedData2, i, j, [255, 0, 255, 0]);
                                break;
                            case 17:
                                updateBlock(encryptedData1, i, j, [0, 0, 255, 255]);
                                updateBlock(encryptedData2, i, j, [0, 255, 255, 0]);
                                break;
                            case 18:
                                updateBlock(encryptedData1, i, j, [255, 0, 0, 255]);
                                updateBlock(encryptedData2, i, j, [0, 255, 0, 255]);
                                break;
                            case 19:
                                updateBlock(encryptedData1, i, j, [255, 0, 0, 255]);
                                updateBlock(encryptedData2, i, j, [0, 0, 255, 255]);
                                break;
                            case 20:
                                updateBlock(encryptedData1, i, j, [0, 255, 0, 255]);
                                updateBlock(encryptedData2, i, j, [255, 0, 0, 255]);
                                break;
                            case 21:
                                updateBlock(encryptedData1, i, j, [0, 255, 0, 255]);
                                updateBlock(encryptedData2, i, j, [0, 0, 255, 255]);
                                break;
                            case 22:
                                updateBlock(encryptedData1, i, j, [0, 0, 255, 255]);
                                updateBlock(encryptedData2, i, j, [255, 0, 0, 255]);
                                break;
                            case 23:
                                updateBlock(encryptedData1, i, j, [0, 0, 255, 255]);
                                updateBlock(encryptedData2, i, j, [0, 255, 0, 255]);
                                break;
                        }
                    }
                }
            }
        }
    }
    return [encryptedData1, encryptedData2];
}

const rawEncryptedData1Link = document.createElement('a');
rawEncryptedData1Link.download = "image-leurre-1.png";
const rawEncryptedData2Link = document.createElement('a');
rawEncryptedData2Link.download = "image-leurre-2.png";

function main() {
    this.disabled = true;
    document.getElementById('output').hidden = false;
    const rawEncryptedData = encryptImage(canvas, context, secretData, lureData1, lureData2);
    context.putImageData(rawEncryptedData[0], 0, 0);
    rawEncryptedData1Link.href = canvas.toDataURL();
    context.putImageData(rawEncryptedData[1], 0, 0);
    rawEncryptedData2Link.href = canvas.toDataURL();
    const imagesFrame = document.getElementById('images');
    if (2 * secretData.width / imagesFrame.clientWidth <= 1 && 2 * secretData.height / imagesFrame.clientHeight <= 1) {
        context.putImageData(rawEncryptedData[0], 0, 0);
        const rawEncryptedImage1 = new Image();
        rawEncryptedImage1.src = canvas.toDataURL();
        context.putImageData(rawEncryptedData[1], 0, 0);
        const rawEncryptedImage2 = new Image();
        rawEncryptedImage2.src = canvas.toDataURL();
        displayImages(rawEncryptedImage1, rawEncryptedImage2);
    }
    else {
        let scaledSecretData = null;
        let scaledEncryptedData = null;
        const ratio = secretData.width / secretData.height;
        if (2 * secretData.width / imagesFrame.clientWidth >= 2 * secretData.height / imagesFrame.clientHeight) {
            scaledSecretData = binarizeImage(canvas, context, secretImage, Math.floor(imagesFrame.clientWidth / 2), Math.floor((imagesFrame.clientWidth / 2) / ratio), document.querySelector('input[name="binarization-algorithm-1"]:checked').value, "secret-image", false);
        }
        else {
            scaledSecretData = binarizeImage(canvas, context, secretImage, Math.floor((imagesFrame.clientHeight / 2) * ratio), Math.floor(imagesFrame.clientHeight / 2), document.querySelector('input[name="binarization-algorithm-1"]:checked').value, "secret-image", false);
        }
        const scaledLureData1 = binarizeImage(canvas, context, lureImage1, scaledSecretData.width, scaledSecretData.height, document.querySelector('input[name="binarization-algorithm-2"]:checked').value, "lure-image-1", false);
        const scaledLureData2 = binarizeImage(canvas, context, lureImage2, scaledSecretData.width, scaledSecretData.height, document.querySelector('input[name="binarization-algorithm-3"]:checked').value, "lure-image-2", false);
        scaledEncryptedData = encryptImage(canvas, context, scaledSecretData, scaledLureData1, scaledLureData2);
        context.putImageData(scaledEncryptedData[0], 0, 0);
        const scaledEncryptedImage1 = new Image();
        scaledEncryptedImage1.src = canvas.toDataURL();
        context.putImageData(scaledEncryptedData[1], 0, 0);
        const scaledEncryptedImage2 = new Image();
        scaledEncryptedImage2.src = canvas.toDataURL();
        displayImages(scaledEncryptedImage1, scaledEncryptedImage2);
    }
}

function enableEncryption() {
    document.getElementById('output').hidden = true;
    let fileInput = null;
    const lureInputs = document.getElementsByClassName('lure-inputs');
    const encryptButton = document.getElementById('encrypt-button');
    let id = null;
    switch (this.type) {
        case "file":
            fileInput = this;
            break;
        case "radio":
            switch (this.name) {
                case "binarization-algorithm-1":
                    fileInput = document.getElementById('file-input-1');
                    break;
                case "binarization-algorithm-2":
                    fileInput = document.getElementById('file-input-2');
                    break;
                case "binarization-algorithm-3":
                    fileInput = document.getElementById('file-input-3');
                    break;
            }
            break;
    }
    switch (fileInput.id) {
        case "file-input-1":
            id = "secret-image";
            break;
        case "file-input-2":
            id = "lure-image-1";
            break;
        case "file-input-3":
            id = "lure-image-2";
            break;
    }
    if (fileInput.files.length > 0) {
        const fileInputs = document.querySelectorAll('input[type=file]');
        let count = 0;
        for (i = 0; i < fileInputs.length; i++) {
            if(fileInputs[i].files.length > 0) {
                count++;
            }
        }
        switch (this.type) {
            case "file":
                const file = this.files[0];
                const reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onloadend = function(event) {
                    const image = new Image();
                    image.src = event.target.result;
                    image.onload = function() {
                        switch (id) {
                            case "secret-image":
                                secretImage = image;
                                secretData = binarizeImage(canvas, context, secretImage, secretImage.width, secretImage.height, document.querySelector('input[name="binarization-algorithm-1"]:checked').value, "secret-image", true);
                                document.getElementById('file-input-2').value = "";
                                document.getElementsByClassName('file-name')[1].innerHTML = "";
                                document.getElementById('file-input-3').value = "";
                                document.getElementsByClassName('file-name')[2].innerHTML = "";
                                for (i = 0; i < lureInputs.length; i++)
                                    lureInputs[i].disabled = false;
                                document.getElementById('lure-image-1').innerHTML = "";
                                document.getElementById('lure-image-2').innerHTML = "";
                                encryptButton.disabled = true;
                                break;
                            case "lure-image-1":
                                lureImage1 = image;
                                lureData1 = binarizeImage(canvas, context, lureImage1, secretImage.width, secretImage.height, document.querySelector('input[name="binarization-algorithm-2"]:checked').value, "lure-image-1", true);
                                if (count === 3)
                                    encryptButton.disabled = false;
                                break;
                            case "lure-image-2":
                                lureImage2 = image;
                                lureData2 = binarizeImage(canvas, context, lureImage2, secretImage.width, secretImage.height, document.querySelector('input[name="binarization-algorithm-3"]:checked').value, "lure-image-2", true);
                                if (count === 3)
                                    encryptButton.disabled = false;
                                break;
                        }
                    }
                }
                break;
            case "radio":
                switch (id) {
                    case "secret-image":
                        secretData = binarizeImage(canvas, context, secretImage, secretImage.width, secretImage.height, document.querySelector('input[name="binarization-algorithm-1"]:checked').value, "secret-image", true);
                        break;
                    case "lure-image-1":
                        lureData1 = binarizeImage(canvas, context, lureImage1, secretImage.width, secretImage.height, document.querySelector('input[name="binarization-algorithm-2"]:checked').value, "lure-image-1", true);
                        break;
                    case "lure-image-2":
                        lureData2 = binarizeImage(canvas, context, lureImage2, secretImage.width, secretImage.height, document.querySelector('input[name="binarization-algorithm-3"]:checked').value, "lure-image-2", true);
                        break;
                }
                if (count === 3)
                    encryptButton.disabled = false;
                break;
        }
    }
    else {
        if (id === "secret-image") {
            document.getElementById('file-input-2').value = "";
            document.getElementsByClassName('file-name')[1].innerHTML = "";
            document.getElementById('file-input-3').value = "";
            document.getElementsByClassName('file-name')[2].innerHTML = "";
            for (i = 0; i < lureInputs.length; i++)
                lureInputs[i].disabled = true;
            document.getElementById('lure-image-1').innerHTML = "";
            document.getElementById('lure-image-2').innerHTML = "";
        }
        document.getElementById(id).innerHTML = "";
        encryptButton.disabled = true;
    }
}