document.addEventListener('DOMContentLoaded', function() {
    const fileInputs = document.querySelectorAll('input[type=file]');
    for (i = 0; i < fileInputs.length; i++) {
        fileInputs[i].addEventListener('change', enableLayering);
    }
});

var image1 = null;
var image2 = null;

function enableLayering() {
    const fileInput = this;
    if (fileInput.files.length > 0) {
        const fileInputs = document.querySelectorAll('input[type=file]');
        let count = 0;
        for (i = 0; i < fileInputs.length; i++) {
            if(fileInputs[i].files.length > 0) {
                count++;
            }
        }
        const file = fileInput.files[0];
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onloadend = function(event) {
            const image = new Image();
            image.src = event.target.result;
            image.onload = function() {
                switch (fileInput.id) {
                    case "file-input-1":
                        image1 = image;
                        break;
                    case "file-input-2":
                        image2 = image;
                        break;
                }
                if (count === 2) {
                    document.getElementById('output').hidden = false;
                    displayImages(image1, image2);
                }
            }
        }
    }
    else {
        document.getElementById('output').hidden = true;
    }
}