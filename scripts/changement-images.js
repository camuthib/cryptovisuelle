document.addEventListener('DOMContentLoaded', function() {
    const fileInputs = document.querySelectorAll('input[type=file]');
    for (i = 0; i < fileInputs.length; i++) {
        fileInputs[i].addEventListener('change', updateName);
    }
});

function updateName() {
    const fileNames = document.getElementsByClassName('file-name');
    const fileInputs = document.querySelectorAll('input[type=file]');
    for (i = 0; i < fileInputs.length; i++) {
        if (fileInputs[i] === this) {
            fileNames[i].innerHTML = "";
            if (this.files.length > 0) {
                fileNames[i].innerHTML = this.files[0].name;
            }
        }
    }
}