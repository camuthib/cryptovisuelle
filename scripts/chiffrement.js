function updateBlock(imageData, x, y, values) {
    secretWidth = imageData.width / 2;
    imageData.data[4 * y + 2 * x] = values[0];
    imageData.data[4 * y + 2 * x + 1] = values[0];
    imageData.data[4 * y + 2 * x + 2] = values[0];
    imageData.data[4 * y + 2 * x + 3] = 255;
    imageData.data[4 * y + 2 * x + 4] = values[1];
    imageData.data[4 * y + 2 * x + 5] = values[1];
    imageData.data[4 * y + 2 * x + 6] = values[1];
    imageData.data[4 * y + 2 * x + 7] = 255;
    imageData.data[4 * y + 8 * secretWidth + 2 * x] = values[2];
    imageData.data[4 * y + 8 * secretWidth + 2 * x + 1] = values[2];
    imageData.data[4 * y + 8 * secretWidth + 2 * x + 2] = values[2];
    imageData.data[4 * y + 8 * secretWidth + 2 * x + 3] = 255;
    imageData.data[4 * y + 8 * secretWidth + 2 * x + 4] = values[3];
    imageData.data[4 * y + 8 * secretWidth + 2 * x + 5] = values[3];
    imageData.data[4 * y + 8 * secretWidth + 2 * x + 6] = values[3];
    imageData.data[4 * y + 8 * secretWidth + 2 * x + 7] = 255;
}