function grayShade(red, green, blue) {
    return Math.round(0.2126 * red + 0.7152 * green + 0.0722 * blue);
}

function otsuMethod(imageData) {
    let grayscaleProbs = new Array(256).fill(0);
    for (i = 0; i < imageData.data.length; i += 4) {
        grayscaleProbs[grayShade(imageData.data[i], imageData.data[i + 1], imageData.data[i + 2])] += 1;
    }
    let mT = 0;
    for (i = 0; i < grayscaleProbs.length; i++) {
        grayscaleProbs[i] /= (imageData.data.length / 4);
        mT += i * grayscaleProbs[i];
    }
    let w0 = 0;
    let w1 = 1;
    let sum0 = 0;
    let threshold = 1;
    let maxVariance = 0;
    for (t = 1; t < grayscaleProbs.length; t++) {
        w0 += grayscaleProbs[t - 1];
        w1 = 1 - w0;
        sum0 += (t - 1) * grayscaleProbs[t - 1];
        let m0 = sum0 / w0;
        let m1 = (mT - w0 * m0) / w1;
        let variance = w0 * w1 * Math.pow(m0 - m1, 2);
        if (variance > maxVariance) {
            threshold = t;
            maxVariance = variance;
        }
    }
    for (i = 0; i < imageData.data.length; i += 4) {
        const newPixelValue = thresholding(grayShade(imageData.data[i], imageData.data[i + 1], imageData.data[i + 2]), threshold);
        imageData.data[i] = newPixelValue;
        imageData.data[i + 1] = newPixelValue;
        imageData.data[i + 2] = newPixelValue;
        imageData.data[i + 3] = 255;
    }
}

function thresholding(shade, threshold) {
    let v = 0;
    if (shade >= threshold) {
        v = 255;
    }
    return v;
}

function floydSteinberg(imageData, threshold) {
    for (j = 0; j < imageData.data.length; j += 4 * imageData.width) {
        for (i = 0; i < 4 * imageData.width; i += 4) {
            const oldPixel = [imageData.data[j + i], imageData.data[j + i + 1], imageData.data[j + i + 2]];
            const oldPixelShade = grayShade(oldPixel[0], oldPixel[1], oldPixel[2]);
            const newPixelValue = thresholding(oldPixelShade, threshold);
            imageData.data[j + i] = newPixelValue;
            imageData.data[j + i + 1] = newPixelValue;
            imageData.data[j + i + 2] = newPixelValue;
            imageData.data[j + i + 3] = 255;
            const error = oldPixelShade - newPixelValue;
            if (i + 4 < 4 * imageData.width) {
                imageData.data[j + i + 4] += error * 7 / 16;
                imageData.data[j + i + 5] += error * 7 / 16;
                imageData.data[j + i + 6] += error * 7 / 16;
            }
            if (j + 4 * imageData.width < imageData.data.length) {
                if (i - 4 >= 0) {
                    imageData.data[j + 4 * imageData.width + i - 4] += error * 3 / 16;
                    imageData.data[j + 4 * imageData.width + i - 3] += error * 3 / 16;
                    imageData.data[j + 4 * imageData.width + i - 2] += error * 3 / 16;
                }
                imageData.data[j + 4 * imageData.width + i] += error * 5 / 16;
                imageData.data[j + 4 * imageData.width + i + 1] += error * 5 / 16;
                imageData.data[j + 4 * imageData.width + i + 2] += error * 5 / 16;
                if (i + 4 < 4 * imageData.width) {
                    imageData.data[j + 4 * imageData.width + i + 4] += error * 1 / 16;
                    imageData.data[j + 4 * imageData.width + i + 5] += error * 1 / 16;
                    imageData.data[j + 4 * imageData.width + i + 6] += error * 1 / 16;
                }
            }
        }
    }
}

function binarization(imageData, algorithm) {
    switch (algorithm) {
        case "global-thresholding":
            otsuMethod(imageData);
            break;
        case "error-diffusion":
            floydSteinberg(imageData, 128);
            break;
    }
}

function binarizeImage(canvas, context, image, width, height, algorithm, id, display) {
    canvas.width = width;
    canvas.height = height;
    context.drawImage(image, 0, 0, width, height);
    const imageData = context.getImageData(0, 0, canvas.width, canvas.height);
    binarization(imageData, algorithm);
    if (display) {
        context.putImageData(imageData, 0, 0);
        document.getElementById(id).innerHTML = "";
        const imageDataURL = canvas.toDataURL();
        const binaryImage = new Image();
        binaryImage.src = imageDataURL;
        document.getElementById(id).appendChild(binaryImage);
    }
    return imageData;
}